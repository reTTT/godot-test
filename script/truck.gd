
extends Node

const D_BACK = 0
const D_FRONT = 1
const D_FULL = 2

var wheel_rear = null
var wheel_front = null

var speed = 0

export var acceleration = 0.5
export var max_speed = 10
export(int,"D_BACK","D_FRONT","D_FULL") var drive = D_FULL


func add_speed(a):
	speed = clamp(speed+a, -max_speed, max_speed)


func move(dt):
	if drive == D_BACK || drive == D_FULL:
		wheel_rear.set_angular_velocity(speed)
		
	if drive == D_FRONT || drive == D_FULL:
		wheel_front.set_angular_velocity(speed)


func _fixed_process(dt):
	
	var brake = Input.is_action_pressed("ui_accept")
	if brake:
		wheel_rear.set_angular_velocity(0)
		wheel_front.set_angular_velocity(0)
	else:
		var walk_left = Input.is_action_pressed("ui_left")
		var walk_right = Input.is_action_pressed("ui_right")

		if walk_right:
			add_speed(acceleration)
		elif walk_left:
			add_speed(-acceleration)
		else:
			speed = 0

		if speed != 0:
			move(dt)


func _ready():

	wheel_rear = get_node("wheel_rear")
	wheel_front = get_node("wheel_front")
	
	set_fixed_process(true)

