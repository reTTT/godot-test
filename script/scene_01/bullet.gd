extends RigidBody2D


export var force = 700

var spawn = null
var need_reset = false

func reset():
	need_reset = true


func _integrate_forces(s):
	s.set_angular_velocity(0)
	s.set_linear_velocity(Vector2(force, 0))


func _fixed_process(dt):
	
	if need_reset == true:
		set_active(false)
		set_mode(RigidBody2D.MODE_KINEMATIC)
		set_linear_velocity(Vector2(0, 0))
		set_pos(spawn)
		set_mode(RigidBody2D.MODE_RIGID)
		set_active(true)
		need_reset = false

	#set_applied_force(Vector2(force, 0))


func _ready():
	spawn = get_pos()
	set_fixed_process(true)

