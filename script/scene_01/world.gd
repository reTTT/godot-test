
extends Node

# member variables here, example:
# var a=2
# var b="textvar"

var bullet = null
var generator = null


func _ready():
	bullet = get_node("/root/world/bullet")
	generator = get_node("/root/world/img_generator")
	

func _on_btn_reset_pressed():
	bullet.reset()
	generator.reset()
	
