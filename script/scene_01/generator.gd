extends Node


var img = preload("res://textures/godot_logo4x4.png") 
var pref_point = preload("res://prefabs/scene_01/point4x4.scn")
const size = 4

func create():
	var data = img.get_data()
	var h = data.get_height()
	var w = data.get_width()
	
	var count = 0;
	var offset = Vector2((800 - w*size)/2, (600 - h*size)/2)
	
	for y in range(data.get_height()):
		for x in range(data.get_width()):
			var color = data.get_pixel(x, y)
			if color.a < 0.8:
				continue
			
			var point = pref_point.instance()
			point.set_pos(Vector2(x*size, y*size)+offset)
			var spr = point.get_node("Sprite")
			spr.set_modulate(color)
			add_child(point)
			count += 1
	
	print(str(count))

func reset():
	var children = get_children()
	for child in children:
		remove_child(child)
		
	create()

func _ready():
	create()
